﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Metoda, která po kliknutí zavolá modální okno s textem 'ahoj'
        /// </summary>
        /// <param name="sender">odesílate, to je form</param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ahoj");
        }

        /// <summary>
        /// Metoda, která není zatím implementována
        /// </summary>
        /// <param name="p1">řetězec </param>
        /// <param name="p2">číslo </param>
        public void MojeMetoda(string p1,int p2)
        {
            throw new Exception("Není vytvořené");
        }
    }
}
