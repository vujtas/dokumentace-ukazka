﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    /// <summary>
    /// Moje třída, která něco dělá
    /// </summary>
    public class MojeTrida
    {
        /// <summary>
        /// Konstruktor, který naplní parametry
        /// </summary>
        /// <param name="mojeId"></param>
        /// <param name="mojePrezdivka"></param>
        public MojeTrida(int mojeId, string mojePrezdivka)
        {
            MojeId = mojeId;
            MojePrezdivka = mojePrezdivka;
        }

        /// <summary>
        /// Muj parametr ktery je int
        /// </summary>
        public int MojeId { get; private set; }

        /// <summary>
        /// MUj parametr ktery je string
        /// </summary>
        public string MojePrezdivka { get;  private set; }

        /// <summary>
        /// Metoda, ktera něco dělá
        /// </summary>
        /// <returns>co navrací aková metoda</returns>
        public int MojeMetoda()
        {
            return -1;
        }
    }
}
